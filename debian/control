Source: libcatmandu-dbi-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libcatmandu-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libdbi-perl <!nocheck>,
                     libjson-perl <!nocheck>,
                     libmoo-perl <!nocheck>,
                     libmoox-aliases-perl <!nocheck>,
                     libnamespace-clean-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-dbi-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-dbi-perl.git
Homepage: https://metacpan.org/release/Catmandu-DBI
Rules-Requires-Root: no

Package: libcatmandu-dbi-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcatmandu-perl,
         libdbi-perl,
         libjson-perl,
         libmoo-perl,
         libmoox-aliases-perl,
         libnamespace-clean-perl
Recommends: libdbd-mysql-perl,
            libdbd-pg-perl,
            libdbd-sqlite3-perl
Description: Catmandu tools to communicate with DBI based interfaces
 Catmandu::DBI provides tools that communicate with DBI based interfaces
 .
 The following modules are provided:
  Catmandu::DBI
  Catmandu::Importer::DBI
  Catmandu::Serializer::json_string
  Catmandu::Store::DBI
  Catmandu::Store::DBI::Bag
 .
 More information on Perl DBI
  https://dbi.perl.org
 .
 Catmandu provides a command line client and a Perl API to ease the export (E)
 transformation (T) and loading (L) of data into databases or data file, ETL in
 short.
 .
 Catmandu is specialized in processing, converting, creating library metadata
 from various input sources.
 .
 Catmandu can process: MARC, MAB, XML, Aleph X-services, BagIt, BibTeX, EAD,
 LIDO, JSON Markdown, MODS, OAI-PMH, ORCID, PLoS, PNX, RDF, RIS, SRU, XML, XLS,
 XSD , YAML, Z39.50 and many other formats.
